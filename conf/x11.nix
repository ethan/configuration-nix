{ config, lib, pkgs, ... }:

{
  nixpkgs.config.packageOverrides = pkgs: {
        polybar = pkgs.polybar.override {
          i3Support = true;
        };
  };

  environment.systemPackages = with pkgs; [
    unclutter
    awesome
    # way-cooler
    wayland
    xwayland
    sway
    libnotify
    rofi
    gparted
    xorg.xdpyinfo
    wpa_supplicant_gui
    xlibs.xmessage
	  xlibs.xev
	  xlibs.xinput
    xlibs.xmodmap
    i3lock-fancy
    xidlehook
    xsecurelock
    xlockmore
    scrot
    dunst
    libinput
    xbindkeys
    xdotool
    tk
    # qtstyleplugins
    lxappearance-gtk3
    numix-cursor-theme
    numix-gtk-theme
    numix-icon-theme
    numix-solarized-gtk-theme
    numix-sx-gtk-theme
    arc-icon-theme
    adapta-gtk-theme
    xorg.xcursorthemes
    paper-icon-theme
    polybar
  ];

  fonts = {
    fonts = with pkgs; [
      dejavu_fonts
      source-code-pro
      source-sans-pro
      source-serif-pro
      powerline-fonts
      fira-code
      fira-code-symbols
      font-awesome-ttf
      # monoid
      sudo-font
      noto-fonts
      noto-fonts-emoji 
      roboto
      mplus-outline-fonts
      siji
      hermit
      # nerdfonts
    ];
    # fontconfig = {
    #   penultimate.enable = false;
    #   defaultFonts = {
    #     monospace = [ "Source Code Pro" ];
    #     sansSerif = [ "Source Sans Pro" ];
    #     serif     = [ "Source Serif Pro" ];
    #   };
    # };
  };
  # KDE complains if power management is disabled (to be precise, if
  # there is no power management backend such as upower).
  powerManagement.enable = true;

  # Enable the X11 windowing system.
  # services.xserver.enable = true;
  # services.xserver.layout = "us";
  services.xserver = {
    enable = true;

    displayManager.defaultSession = "none+i3";

    displayManager.sddm = {
      enable = true;
      theme = "adapta";
    };

    desktopManager = {
      xterm.enable = false;
    };

    desktopManager.plasma5 = {
      enable = true;
    };

    desktopManager.lxqt.enable = true;

    windowManager.i3 = {
      enable = true;
      package = pkgs.i3-gaps;
      extraPackages = with pkgs; [
        dmenu
        i3status
        i3status-rust
        i3lock
        file
        polybar
        python3Packages.py3status
        # (python3Packages.py3status.overrideAttrs (oldAttrs: {
        #   propagatedBuildInputs = [ python3Packages.pytz python3Packages.tzlocal python3Packages.pydbus python3Packages.pygobject3 python3Packages.pyserial python3Packages.dateutil python3Packages.pgkresources];
        #   #propagatedBuildInputs = [ python3Packages.pytz python3Packages.requests python3Packages.tzlocal python3Packages.i3ipc python3Packages.pydbus python3Packages.pygobject3 python3Packages.pyserial python3Packages.dateutil python3Packages.pgkresources];
        # }))
      ];
    };

    windowManager.bspwm.enable = true;
    windowManager.stumpwm.enable = true;
    windowManager.exwm.enable = true;
    windowManager.leftwm.enable = true;

    windowManager.awesome = {
      enable = true;
      # luaModules = {
      # };
    };

    wacom.enable = true;


    xkbOptions = "grp:alt_space_toggle, ctrl:nocaps";

    libinput = {
      enable = true;
      tapping = false;
      disableWhileTyping = true;
      scrollMethod = "twofinger";
      naturalScrolling = true; 
      accelProfile = "adaptive";
      clickMethod = "clickfinger";
    };

    synaptics.enable = false;
    config = ''
      Section "InputClass"
        Identifier     "Enable libinput for TrackPoint"
        MatchIsPointer "on"
        Driver         "libinput"
      EndSection
      Section "InputClass"
        Identifier "ELECOM ELECOM TrackBall Mouse"
        Driver "libinput"
        Option "ScrollMethod" "button"
        Option "ScrollButton" "12" # Or whatever button suits your needs
      EndSection
    '';

    # xautolock = {
      # enable = true;
      # locker = "\"${pkgs.i3lock-fancy}/bin/i3lock-fancy -gp -- scrot -z\"";
      # nowlocker = "\"${pkgs.i3lock-fancy}/bin/i3lock-fancy -gp -- scrot -z\"";
      # notifier = "\"${pkgs.libnotify}/bin/notify-send 'Locking in 10 seconds'\"";
    # };
  };

  services.compton = {
    enable = true;
  };
  # programs.way-cooler.enable = true;

  programs.sway = {
    enable = true;
  };

#  systemd.user.services."dunst" = {
#    enable = true;
#    description = "";
#    wantedBy = [ "default.target" ];
#    serviceConfig.Restart = "always";
#    serviceConfig.RestartSec = 2;
#    serviceConfig.ExecStart = "${pkgs.dunst}/bin/dunst";
#  }; 

#  services.redshift = {
#	  enable = true;
#	  provider = "geoclue2";
#    temperature.night = 3500;
#    brightness.night = "0.8";
#  };

  # services.fractalart.enable = true;
  services.unclutter.enable = true;

  environment.etc."X11/xorg.conf.d/50-jabra.conf".text = ''
  Section "InputClass"
    Identifier "Jabra"
    MatchProduct "GN Netcom A/S Jabra EVOLVE LINK"
    Option "ButtonMapping" "0 0 0 0 0 0 0 0 0 0 0 0"
  EndSection
'';
}
