{ config, lib, pkgs, ... }:

{
  services.printing.enable = true;
  services.printing.drivers = [
    pkgs.hplip
    pkgs.brlaser
    pkgs.hplipWithPlugin
    pkgs.epson-escpr
    pkgs.fxlinuxprint
  ];
}
