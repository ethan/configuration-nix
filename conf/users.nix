{ config, lib, pkgs, ... }:

{
  users.extraUsers.root = {
    shell = pkgs.zsh;
  };
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.extraUsers.ethan = {
    isNormalUser = true;
    uid = 1000;
    extraGroups = ["wheel" "docker" "fcron" "audio" "scanner" "lp" "plugdev" "video" "networkmanager" "netdev"];
    shell = pkgs.zsh;
    # shell = pkgs.fish;
  };
}
