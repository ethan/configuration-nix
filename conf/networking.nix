{ config, lib, pkgs, ... }:

{
  # networking.wireless.enable = true;
  # hardware.bluetooth.enable = true;
  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 22 80 8080 8081 8088 8082 3000 9000 19000 19001 19002  ];
  networking.firewall.allowedUDPPorts = [ 5353 ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;
  # Avahi for printer discovery, etc.
  services.avahi.enable = true;
  services.avahi.nssmdns = true;
  services.resolved = {
    enable = true;
    fallbackDns = [ "208.67.222.222" "208.67.220.220" ];
    # fallbackDns = [ "208.67.222.222" "208.67.220.220" "2620:119:35::35" "2620:119:53::53"];
    llmnr = "false";
  };
  # services.resolved.fallbackDns = ["8.8.8.8" "4.4.8.8"];
  # services.resolved.fallbackDns = ["8.8.8.8" "193.138.219.228"];
  # services.resolved.fallbackDns = ["8.8.8.8" "208.67.222.222"];
  # services.dnsmasq = {
  #   enable = true;
  #   servers = ["8.8.8.8" "193.138.219.228"];
  # } ;
  # networking.dnsSingleRequest = true;
  networking.extraHosts = ''
    192.168.89.89  drupalvm.test 
    127.0.0.1 admin.localhost website.localhost 
  '';
  # Enable CUPS to print documents.
  services.printing.enable = true;

  # services.stubby = {
  #   enable = true;
  #   listenAddresses = [ "127.0.0.1@8053" "0::1@8053" ];
  # };
  # services.unbound = {
  #   enable = true;
  #   allowedAccess = [ "127.0.0.1/24" "10.99.148.127/32" "fc00:bbbb:bbbb:bb01::947f/128"];
  #   extraConfig = ''
  #     hide-identity: yes
  #     hide-version: yes
  #     qname-minimisation: yes
  #     harden-short-bufsize: yes
  #     harden-large-queries: yes
  #     harden-glue: yes
  #     harden-dnssec-stripped: yes
  #     harden-below-nxdomain: yes
  #     harden-referral-path: yes
  #     do-not-query-localhost: no
  #     forward-zone:
  #     name: "."
  #     forward-addr: 127.0.0.1@8053
  #     '';
  #     # forward-zone:
  #     # name: "."
  #     # forward-addr: 127.0.0.1@8053
  # }; 

  # Enable Wireguard
  # networking.nat.enable = true;
  # networking.nat.externalInterface = "eth0";
  # networking.nat.internalInterfaces = [ "wg0" ]; 
  # networking.wireguard.interfaces = {
  #   # "wg0" is the network interface name. You can name the interface arbitrarily.
  #   wg0 = {
  #     # Determines the IP address and subnet of the client's end of the tunnel interface.
  #     ips = [ "10.99.64.124/32" "fc00:bbbb:bbbb:bb01::407c/128" ];
  #     # ips = [ "10.99.148.127/32" ];

  #     # Path to the private key file.
  #     #
  #     # Note: The private key can also be included inline via the privateKey option,
  #     # but this makes the private key world-readable; thus, using privateKeyFile is
  #     # recommended.
  #     privateKey = "PhKlTefFFo8BoUcnZTxKVOdS1z98HGlgxauy15Ey5fw=";
  #     # privateKeyFile = "/home/ethan/wireguard-keys/private";
  #     postSetup = "resolvectl domain wg0 \"~.\"; resolvectl dns wg0 193.138.219.228; resolvectl dnssec wg0 yes";

  #     peers = [
  #       # For a client configuration, one peer entry for the server will suffice.
  #       {
  #         # Public key of the server (not a file path).
  #         publicKey = "BQVUyn4F0NlbALK0ksCi4pY9d6/adMaXPdtjKgoL41E=";

  #         # Forward all the traffic via VPN.
  #         allowedIPs = [ "0.0.0.0/0" "::0/0" ];
  #         # allowedIPs = [ "0.0.0.0/0" ];

  #         # Set this to the server IP and port.
  #         endpoint = "198.144.156.48:51820";

  #         # Send keepalives every 25 seconds. Important to keep NAT tables alive.
  #         persistentKeepalive = 25;
  #       }
  #     ];
  #   };
  # };

# services.openvpn.servers = {
#   mullvadVPN  = { config = '' config /root/openvpn/mullvad_sg.conf ''; };
# };

boot.extraModulePackages = [ config.boot.kernelPackages.wireguard ];
environment.systemPackages = [ pkgs.wireguard pkgs.wireguard-tools ];
services.keybase.enable = true;
services.kbfs.enable = true;
}
