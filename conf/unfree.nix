{ config, pkgs, ... }:

{
  nixpkgs.config = {
    allowUnfree = true;
  };
  boot.extraModulePackages = [ config.boot.kernelPackages.exfat-nofuse ]; 
}
