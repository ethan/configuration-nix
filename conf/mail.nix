{ config, lib, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    neomutt
    notmuch
    mu
    w3m
    abook
  ];
}
