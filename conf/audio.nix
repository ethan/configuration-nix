{ config, lib, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    # sunvox
    alsaPlugins
  ];
  # imports = [
  #  "/opt/musnix"
  # ];

  # Remember to comment uncomment cpuFreqGovernor in hardware nix.
  # musnix.enable = true;

  boot.initrd.kernelModules = [ "snd-aloop" "snd-seq" "snd-rawmidi" ];

  security.pam.loginLimits = [
    { domain = "@audio"; item = "memlock"; type = "-"; value = "unlimited"; }
    { domain = "@audio"; item = "rtprio"; type = "-"; value = "99"; }
    { domain = "@audio"; item = "nofile"; type = "soft"; value = "99999"; }
    { domain = "@audio"; item = "nofile"; type = "hard"; value = "99999"; }
  ];

#   systemd.user.services.pulseaudio.environment = {
#     JACK_PROMISCUOUS_SERVER = "jackaudio";
#   };
# 
#   services.jack = {
#     jackd.enable = true;
#     # support ALSA only programs via ALSA JACK PCM plugin
# #     alsa.enable = false;
# #     # support ALSA only programs via loopback device (supports programs like Steam)
# #     loopback = {
# #       enable = true;
# #       # buffering parameters for dmix device to work with ALSA only semi-professional sound programs
# #       dmixConfig = ''
# #         period_size 2048
# #       '';
# #     };
#   };

  users.users.ethan.extraGroups = [ "audio" "jackaudio" "realtime"];
}


