{ config, lib, pkgs, ... }:

{
  services.fcron.enable = true;
  services.fcron.systab = ''
  */5 * * * *             ethan       . /etc/profile; /home/ethan/.nix-profile/bin/gcalcli remind
  '';
}
