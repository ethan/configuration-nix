{ config, lib, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    tmux
    zsh
    # zsh-powerlevel9k
    antigen
    oh-my-zsh
    zsh-autosuggestions
    nix-zsh-completions
    zsh-syntax-highlighting
    # zsh-fast-syntax-highlighting
    starship
    autojump
  ];
  # TODO determine how this should change for zsh
  programs.bash.enableCompletion = true;
  # programs.zsh.promptInit = "source ${pkgs.zsh-powerlevel9k}/share/zsh-powerlevel9k/powerlevel9k.zsh-theme";

  environment.shellInit = ''
    export GPG_TTY="$(tty)"
    gpg-connect-agent /bye
    export SSH_AUTH_SOCK="/run/user/$UID/gnupg/S.gpg-agent.ssh"
  '';

  programs.fish.enable = true;
  # Enable Oh-my-zsh
  programs.zsh = {
    enable = true;
    # enableCompletion = true;
    # autosuggestions.enable = true;
    # syntaxHighlighting.enable = true; 
    interactiveShellInit = ''
  
ANTIGEN_CACHE=false
source ${pkgs.antigen}/share/antigen/antigen.zsh

  POWERLEVEL9K_SHORTEN_STRATEGY="truncate_middle"
  POWERLEVEL9K_SHORTEN_DIR_LENGTH=4
  POWERLEVEL9K_PROMPT_ON_NEWLINE=true
  POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(context dir virtualenv vcs status)
  POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(command_execution_time time)
  POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX=""
  POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX='%K{white} %k%F{white}\uE0B0%f '
  POWERLEVEL9K_PROMPT_ADD_NEWLINE=true
  POWERLEVEL9K_ALWAYS_SHOW_CONTEXT=true
  POWERLEVEL9K_CONTEXT_ROOT_FOREGROUND="black"
  POWERLEVEL9K_CONTEXT_ROOT_BACKGROUND="white"
  POWERLEVEL9K_CONTEXT_DEFAULT_FOREGROUND="black"
  POWERLEVEL9K_CONTEXT_DEFAULT_BACKGROUND="white"
  POWERLEVEL9K_STATUS_OK=false
  POWERLEVEL9K_EXECUTION_TIME_ICON=""

   antigen use oh-my-zsh
   # antigen theme bhilburn/powerlevel9k powerlevel9k
   # antigen theme https://github.com/caiogondim/bullet-train-oh-my-zsh-theme bullet-train
   # antigen theme denysdovhan/spaceship-prompt

   antigen bundle git           # support for git
   antigen bundle git-extras    # ?
   antigen bundle python        # support for python
   antigen bundle history       # [h] give history, [hsi] allow grep in history
   antigen bundle pip           # Autocompletion on pip
   antigen bundle npm           # Same for npm
   antigen bundle rvm           # Same for rvm
   antigen bundle taskwarrior
   antigen bundle sudo
   antigen bundle extract
   antigen bundle history-substring-search
   antigen bundle zdharma/fast-syntax-highlighting
   antigen bundle zsh-users/zsh-autosuggestions

   # Additionnal completion definition
   antigen bundle zsh-users/zsh-completions src
   antigen apply
   eval "$(starship init zsh)"

   #== History ================
   setopt APPEND_HISTORY          # history appends to existing file
   setopt HIST_FIND_NO_DUPS       # history search finds once only
   setopt HIST_IGNORE_ALL_DUPS    # remove all earlier duplicate lines
   setopt HIST_REDUCE_BLANKS      # trim multiple insgnificant blanks in history
   setopt HIST_NO_STORE           # remove the history (fc -l) command from the history when invoked
   # HISTFILE=$HOME/.zsh/history    # history file location
   # HISTSIZE=1000000               # number of history lines kept internally
   # SAVEHIST=1000000               # max number of history lines saved
   '';
	  ohMyZsh = {
	    # enable = true;
	    plugins = [ 
		"git"
                "autojump"
		"sudo"
                "take"
		"docker"
		"vi-mode"
		"taskwarrior"
		"sudo"
		"extract"
		"encode64"
		"dirhistory" 
		"common-aliases" 
		"catimg" 
		"history-substring-search" 
		"history" 
		"zsh-nix-shell" 
		"nix-zsh-completions" 
		"zsh-autosuggestions" 
		"zsh-syntax-highlighting" 
		"fast-syntax-highlighting"
];
	    theme = "blinks";
	  };
  };
}
