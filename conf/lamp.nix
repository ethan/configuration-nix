{ config, lib, pkgs, ... }:

{
  services.mysql.enable = true;
  services.mysql.package = pkgs.mariadb;
  environment.systemPackages = with pkgs; [
    php
    php72Packages.xdebug
    php72Packages.composer
    php72Packages.phpcs
    mariadb
  ];
  environment.etc."php.d/php.ini".text = ''
    memory_limit = 512M
    error_log = /var/log/php_error_log
    sendmail_path = "/run/wrappers/bin/sendmail -t"
    sendmail_from = "ethan@colab.coop"
    upload_max_filesize = 24M
    post_max_size = 32M
    max_execution_time = 300
  '';
  nixpkgs.config.php = {
    mysqlnd = true;
    xsl = true;
  };
  # Enable postfix to install sendmail
  services.postfix.enable = true;
  services.httpd = {
    enable = true;
    user = "ethan";
    adminAddr = "ethan@colab.coop";
    documentRoot = "/var/www";
    enablePHP = true;
    phpOptions = ''
      zend_extension="${pkgs.php72Packages.xdebug}/lib/php/extensions/xdebug.so"
      zend_extension_ts="${pkgs.php72Packages.xdebug}/lib/php/extensions/xdebug.so"
      zend_extension_debug="${pkgs.php72Packages.xdebug}/lib/php/extensions/xdebug.so"

      [XDebug]
      xdebug.remote_enable=true
      xdebug.remote_host=127.0.0.1
      xdebug.remote_port=9000
      xdebug.remote_handler=dbgp
      xdebug.profiler_enable=0
      xdebug.profiler_output_dir="/tmp/xdebug"
      xdebug.remote_mode=req
      xdebug.idekey=xdebug
    '';
    virtualHosts = [
      {
        hostName = "test.sevgen-d8.test";
        documentRoot = "/home/ethan/projects/7gen/sevgen-d8-cms-test/docroot";
        extraConfig = ''
        ServerAlias *.test.sevgen-d8.test
        DirectoryIndex index.php
        <Directory "/home/ethan/projects/7gen/sevgen-d8-cms-test/docroot">
        AllowOverride All
        </Directory>
        '';
      }
      {
        hostName = "sevgen-d8.test";
        documentRoot = "/home/ethan/projects/7gen/sevgen-d8-cms/docroot";
        extraConfig = ''
          DirectoryIndex index.php
          ServerAlias *.sevgen-d8.test
          ServerAlias local.seventhgeneration.com
          ServerAlias www.seventhgeneration.com
          <Directory "/home/ethan/projects/7gen/sevgen-d8-cms/docroot">
            AllowOverride All
          </Directory>
        '';
      }
      {
        hostName = "sevgen-d7.test";
        documentRoot = "/home/ethan/projects/7gen/sevgen-d7";
        extraConfig = ''
          DirectoryIndex index.php
          <Directory "/home/ethan/projects/7gen/sevgen-d7">
            AllowOverride All
          </Directory>
        '';
      }
      {
        hostName = "sevgen-d7-files.test";
        documentRoot = "/home/ethan/projects/7gen/sevgen-d7-files";
        extraConfig = ''
          <Directory "/home/ethan/projects/7gen/sevgen-d7-files">
            AllowOverride All
          </Directory>
        '';
      }
      {
      hostName = "aaas.test";
      documentRoot = "/home/ethan/projects/aaas/dotorg/web";
      extraConfig = ''
      DirectoryIndex index.php
      <Directory "/home/ethan/projects/aaas/dotorg/web">
      AllowOverride All
      </Directory>
      '';
      }
    ];
  };
    environment.etc."php.d/xdebug.ini".text = ''
      zend_extension="${pkgs.php72Packages.xdebug}/lib/php/extensions/xdebug.so"
      zend_extension_ts="${pkgs.php72Packages.xdebug}/lib/php/extensions/xdebug.so"
      zend_extension_debug="${pkgs.php72Packages.xdebug}/lib/php/extensions/xdebug.so"

      [XDebug]
      xdebug.remote_enable=true
      xdebug.remote_host=127.0.0.1
      xdebug.remote_port=9000
      xdebug.remote_handler=dbgp
      xdebug.profiler_enable=0
      xdebug.profiler_output_dir="/tmp/xdebug"
      xdebug.remote_mode=req
      xdebug.idekey=xdebug
    '';
  networking.extraHosts = "
127.0.0.1 localhost
127.0.0.1 sevgen-d8.test
127.0.0.1 test.sevgen-d8.test
127.0.0.1 fr.sevgen-d8.test
127.0.0.1 sevgen-d8.sevgen-d8.test
127.0.0.1 sevgen-d8.test.sevgen-d8.test
127.0.0.1 ca.sevgen-d8.test
127.0.0.1 ca.test.sevgen-d8.test
127.0.0.1 fr-ca.sevgen-d8.test
127.0.0.1 fr-ca.test.sevgen-d8.test
127.0.0.1 en-uk.sevgen-d8.test
127.0.0.1 en-uk.test.sevgen-d8.test
127.0.0.1 sevgen-d7.test
127.0.0.1 sevgen-d7-files.test
127.0.0.1 local.seventhgeneration.com
127.0.0.1 aaas.test
# 52.62.39.101 api.geddup.com
# 3.106.15.167 api.geddup.com
# 52.87.69.102 new.seventhgeneration.com
# 52.87.69.102 www.seventhgeneration.com
";
  users.users.ethan.extraGroups = ["wwwrun" "mysql"];
}
