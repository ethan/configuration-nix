{ config, pkgs, ... }:

{
  # TODO determine if any of these should apply, cross-reference with hardware.nix
  # boot.kernelParams.i915.enable_rc6 = 1;
  boot.extraModprobeConfig = ''
    # thinkpad acpi
    # options thinkpad_acpi fan_control=1
    # intel graphics
    options i915 modeset=1 i915_enable_rc6=7 i915_enable_fbc=1 lvds_downclock=1 # powersave=0
    # options bbswitch use_acpi_to_detect_card_state=1
    # intel audio
    # options snd-hda-intel index=0 id=PCH
    # options snd-hda-intel index=1 id=HDMI
    # options snd-hda-intel index=0 model=auto vid=8086 pid=8c20
    # options snd-hda-intel index=1 model=auto vid=8086 pid=0c0c
    # options snd slots=snd-hda-intel
    # fix intel wifi-N
    # options iwlwifi 11n_disable=1
    # options iwlwifi swcrypto=1
    # options iwlwifi 11n_disable=8
    # iwlwifi.power_save=Y
    # iwldvm.force_cam=N
  '';

  environment.systemPackages = with pkgs; [
    # Thinkpad stuff
    # thinkfan
    # lm-sensors
  ];

  hardware.trackpoint.enable = true;
  hardware.trackpoint.emulateWheel = true;

  # services.tlp.enable = true;

  # services.xserver.synaptics = {
  #   enable = true;
  #   horizEdgeScroll = false;
  #   horizTwoFingerScroll = true;
  #   palmDetect = true;
  #   twoFingerScroll = true;
  #   vertEdgeScroll = false;
  #   vertTwoFingerScroll = true;
  #   tapButtons = false;
  # };

}
