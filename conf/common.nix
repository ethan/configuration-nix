# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ../conf/unfree.nix
      # ./conf/minimal.nix
      ../conf/base.nix
      ../conf/print.nix
      ../conf/scanner.nix
      ../conf/users.nix
      ../conf/editors.nix
      ../conf/browsers.nix
      # ../conf/lamp.nix
      ../conf/audio.nix
      ../conf/devices.nix
      ../conf/shell.nix
      # ./conf/work.nix
      ../conf/cron.nix
      ../conf/media.nix
      # ./conf/mail.nix
      ../conf/makertools.nix
      # ./conf/design.nix
      ../conf/x11.nix
      ../conf/i18n.nix
      ../conf/networking.nix
      # ../conf/bluetooth.nix
      ../conf/docker.nix
    ];
  
}

