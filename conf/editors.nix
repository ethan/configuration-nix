{ config, lib, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    vim
    bvi
    emacs
    clang
  ];

  services.emacs = {
    enable  = true;
    defaultEditor = true;
    install = true;
  };

}
