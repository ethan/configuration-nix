{ config, lib, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    vim
    clang
    lynx
    wget
    sudo
    gnupg
    git
  ];
  programs.gnupg.agent = { enable = true; enableSSHSupport = true; };
}
