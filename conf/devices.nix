{ config, lib, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    udevil
  ];
  services.udev.packages = [ pkgs.yubikey-personalization
    pkgs.libu2f-host ];
  services.pcscd.enable = true;
}
