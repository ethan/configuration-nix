{ config, lib, pkgs, ... }:

{
  imports = [
    ../lib/sane-extra-config.nix
  ];
  environment.systemPackages = with pkgs; [
    saneFrontends
    xsane
    simple-scan
  ];
  hardware.sane = {
    enable = true;
    extraBackends = [
      pkgs.hplipWithPlugin
      # pkgs.epkowa
    ];
    netConf = ''
192.168.1.192
    '';
    extraConfig."hplip" = ''
      192.168.0.0/24
      192.168.1.0/24
      192.168.11.0/24
    '';
  };
}
