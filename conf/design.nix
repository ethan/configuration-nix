{ config, lib, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    inkscape
    # gimp
  ];

  nixpkgs.config.packageOverrides = pkgs:
  {
    inkscape = pkgs.inkscape.overrideDerivation (old: {
      buildInputs = old.buildInputs ++ [ pkgs.pythonPackages.purepng ];
    });
  };
}
