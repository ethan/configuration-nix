# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  nix.nixPath = [ "nixpkgs=/opt/nixpkgs" "nixos-config=/etc/nixos/configuration.nix" ];

  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./thinkpad.nix
    ];

  fileSystems."/".options = [ "noatime" "nodiratime" "discard" ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "nodev";
  boot.loader.grub.efiSupport = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.grub.gfxmodeEfi = "1024x768";
  boot.loader.grub.enableCryptodisk = true;
  boot.loader.grub.useOSProber = true;
  boot.loader.grub.extraEntries = ''
    # GRUB 2 example
    menuentry "Windows 10" {
      search --fs-uuid --no-floppy --set=root B6D03BCBD03B909B 
      chainloader (\$\{root\})/EFI/Microsoft/Boot/bootmgfw.efi
    }
'';

  boot.plymouth.enable = true;

  boot.initrd.luks.devices = [
    {
      name = "root";
      device = "/dev/disk/by-uuid/0427a0f0-5aa6-417b-96ec-d9385b5bfc26";
      preLVM = true;
      allowDiscards = true;
    }
  ];
  
  networking.hostName = "wabisabi"; # Define your hostname.

  i18n = {
    defaultLocale = "en_US.UTF-8";
  };
  
}
