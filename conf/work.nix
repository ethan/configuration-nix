{ config, lib, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    slack
    todo-txt-cli
    zoom-us
    libreoffice
    dropbox
    (python.withPackages(ps: with ps; [ numpy toolz websocket_client websocket purepng]))
    irssi
    weechat
    yarn2nix
  ];
  nixpkgs.config.packageOverrides = pkgs:
  {
    weechat =
      (pkgs.weechat.override {
	extraBuildInputs = [pkgs.pythonPackages.websocket_client pkgs.pythonPackages.websocket];
        configure = { availablePlugins, ... }: {
          plugins = with availablePlugins; [
            perl tcl ruby guile lua

            (python.withPackages (ps: with ps; [
              websocket_client
              websocket
              dbus-python
            ]))
          ];
        };
      });
  };
}
