{ config, lib, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    # Base
    nixUnstable
    wget 
    glxinfo
    sudo
    udev
    gnupg
    git
    fcron
    udisks
    ntfs3g
    dosfstools
    mtools
    hdparm
    wirelesstools
  ];
  programs.gnupg.agent = { enable = true; enableSSHSupport = true; };
  programs.ssh.startAgent = false;
  programs.java = { enable = true; package = pkgs.jdk11; };
}
