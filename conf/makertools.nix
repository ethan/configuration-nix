{ config, lib, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    platformio
    platformio-udev-rules
    androidsdk-rules
    # openscad
    # inkscape
    # freecad
    # kicad
  ];

  programs.adb.enable = true;
  users.users.ethan.extraGroups = ["adbusers"];

  services.udev.packages = [ pkgs.platformio-udev-rules pkgs.androidsdk-rules ];

  nixpkgs.config.packageOverrides = pkgs:
  {
    androidsdk-rules = pkgs.writeTextFile {
      name = "androidsdk-rules";
      destination = "/etc/udev/rules.d/51-androidsdk-rules";
      text = ''
#Lenovo
SUBSYSTEM=="usb", ATTR{idVendor}=="17EF", MODE="0666", OWNER="ethan"

#LG
SUBSYSTEM=="usb", ATTR{idVendor}=="1004", MODE="0666", OWNER="ethan"

# Google Nexus 5
SUBSYSTEM=="usb", ATTR{idVendor}=="18d1", ATTR{idProduct}=="d00d", MODE="0600", OWNER="ethan"

#Motorola
SUBSYSTEM=="usb", ATTR{idVendor}=="22b8", MODE="0666", OWNER="ethan"

#NEC
SUBSYSTEM=="usb", ATTR{idVendor}=="0409", MODE="0666", OWNER="ethan"

#Nook
SUBSYSTEM=="usb", ATTR{idVendor}=="2080", MODE="0666", OWNER="ethan"

#Nvidia
SUBSYSTEM=="usb", ATTR{idVendor}=="0955", MODE="0666", OWNER="ethan"

#OTGV
SUBSYSTEM=="usb", ATTR{idVendor}=="2257", MODE="0666", OWNER="ethan"

#Pantech
SUBSYSTEM=="usb", ATTR{idVendor}=="10A9", MODE="0666", OWNER="ethan"

#Philips
SUBSYSTEM=="usb", ATTR{idVendor}=="0471", MODE="0666",OWNER="ethan"

#PMC-Sierra
SUBSYSTEM=="usb", ATTR{idVendor}=="04da", MODE="0666", OWNER="ethan"

#Qualcomm
SUBSYSTEM=="usb", ATTR{idVendor}=="05c6", MODE="0666", OWNER="ethan"

#SK Telesys
SUBSYSTEM=="usb", ATTR{idVendor}=="1f53", MODE="0666", OWNER="ethan"

#Samsung
SUBSYSTEM=="usb", ATTR{idVendor}=="04e8", MODE="0666", OWNER="ethan"

#Sharp
SUBSYSTEM=="usb", ATTR{idVendor}=="04dd", MODE="0666", OWNER="ethan"

#Sony Ericsson
SUBSYSTEM=="usb", ATTR{idVendor}=="0fce", MODE="0666", OWNER="ethan"

#Toshiba
SUBSYSTEM=="usb", ATTR{idVendor}=="0930", MODE="0666", OWNER="ethan"

#ZTE
SUBSYSTEM=="usb", ATTR{idVendor}=="19D2", MODE="0666", OWNER="ethan"

# Nexus One (adb)
SUBSYSTEM=="usb", ATTR{idVendor}=="18d1", ATTR{idProduct}=="4e12", MODE="0600", OWNER="ethan"

# Nexus One (fastboot)
SUBSYSTEM=="usb", ATTR{idVendor}=="0bb4", ATTR{idProduct}=="0fff", MODE="0600", OWNER="ethan"

# Nexus S (adb)
SUBSYSTEM=="usb", ATTR{idVendor}=="18d1", ATTR{idProduct}=="4e22", MODE="0600", OWNER="ethan"

# Nexus S (fastboot)
SUBSYSTEM=="usb", ATTR{idVendor}=="18d1", ATTR{idProduct}=="4e20", MODE="0600", OWNER="ethan"

# Xoom (adb)
SUBSYSTEM=="usb", ATTR{idVendor}=="22b8", ATTR{idProduct}=="70a9", MODE="0600", OWNER="ethan"

# Xoom (fastboot)
SUBSYSTEM=="usb", ATTR{idVendor}=="18d1", ATTR{idProduct}=="708c", MODE="0600", OWNER="ethan"

# Galaxy Nexus (adb)
SUBSYSTEM=="usb", ATTR{idVendor}=="04e8", ATTR{idProduct}=="6860", MODE="0600", OWNER="ethan"

# Galaxy Nexus (fastboot)
SUBSYSTEM=="usb", ATTR{idVendor}=="18d1", ATTR{idProduct}=="4e30", MODE="0600", OWNER="ethan"

# PandaBoard (adb)
SUBSYSTEM=="usb", ATTR{idVendor}=="0451", ATTR{idProduct}=="d101", MODE="0600", OWNER="ethan"

# PandaBoard (fastboot)
SUBSYSTEM=="usb", ATTR{idVendor}=="0451", ATTR{idProduct}=="d022", MODE="0600", OWNER="ethan"

# PandaBoard (usbboot)
SUBSYSTEM=="usb", ATTR{idVendor}=="0451", ATTR{idProduct}=="d00f", MODE="0600", OWNER="ethan"

# PandaBoard ES (adb)
SUBSYSTEM=="usb", ATTR{idVendor}=="18d1", ATTR{idProduct}=="d002", MODE="0600", OWNER="ethan"

# PandaBoard ES (usbboot)
SUBSYSTEM=="usb", ATTR{idVendor}=="0451", ATTR{idProduct}=="d010", MODE="0600", OWNER="ethan"

# Nexus 7 (adb)
SUBSYSTEM=="usb", ATTR{idVendor}=="18d1", ATTR{idProduct}=="4e42", MODE="0600", OWNER="ethan"

# Nexus 7 (fastboot)
SUBSYSTEM=="usb", ATTR{idVendor}=="18d1", ATTR{idProduct}=="4e40", MODE="0600", OWNER="ethan"

# Nexus 10 (adb)
SUBSYSTEM=="usb", ATTR{idVendor}=="18d1", ATTR{idProduct}=="4ee2", MODE="0600", OWNER="ethan"

# Nexus 10 (fastboot)
SUBSYSTEM=="usb", ATTR{idVendor}=="18d1", ATTR{idProduct}=="4ee0", MODE="0600", OWNER="ethan"
'';
    };
    platformio-udev-rules = pkgs.writeTextFile {
      name = "platformio-udev-rules";
      text = ''
# Copyright (c) 2014-present PlatformIO <contact@platformio.org>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# INSTALLATION
#

# UDEV Rules for PlatformIO supported boards, http://platformio.org/boards
#
# The latest version of this file may be found at:
# 	https://github.com/platformio/platformio-core/blob/develop/scripts/99-platformio-udev.rules
#
# This file must be placed at:
# 	/etc/udev/rules.d/99-platformio-udev.rules    (preferred location)
#   	or
# 	/lib/udev/rules.d/99-platformio-udev.rules    (req'd on some broken systems)
#
# To install, type this command in a terminal:
# 	sudo cp 99-platformio-udev.rules /etc/udev/rules.d/99-platformio-udev.rules
#
# Restart "udev" management tool:
#	sudo service udev restart
#		or
# 	sudo udevadm control --reload-rules
#	sudo udevadm trigger
#
# Ubuntu/Debian users may need to add own “username” to the “dialout” group if
# they are not “root”, doing this issuing a
#   sudo usermod -a -G dialout $USER
#   sudo usermod -a -G plugdev $USER
#
# After this file is installed, physically unplug and reconnect your board.

# CP210X USB UART
SUBSYSTEMS=="usb", ATTRS{idVendor}=="10c4", ATTRS{idProduct}=="ea60", MODE:="0666"

# FT232R USB UART
SUBSYSTEMS=="usb", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6001", MODE:="0666"

# Prolific Technology, Inc. PL2303 Serial Port
SUBSYSTEMS=="usb", ATTRS{idVendor}=="067b", ATTRS{idProduct}=="2303", MODE:="0666"

# QinHeng Electronics HL-340 USB-Serial adapter
SUBSYSTEMS=="usb", ATTRS{idVendor}=="1a86", ATTRS{idProduct}=="7523", MODE:="0666"

# Arduino boards
SUBSYSTEMS=="usb", ATTRS{idVendor}=="2341", ATTRS{idProduct}=="[08][02]*", MODE:="0666"
SUBSYSTEMS=="usb", ATTRS{idVendor}=="2a03", ATTRS{idProduct}=="[08][02]*", MODE:="0666"

# Arduino SAM-BA
ATTRS{idVendor}=="03eb", ATTRS{idProduct}=="6124", ENV{ID_MM_DEVICE_IGNORE}="1"
ATTRS{idVendor}=="03eb", ATTRS{idProduct}=="6124", ENV{MTP_NO_PROBE}="1"
SUBSYSTEMS=="usb", ATTRS{idVendor}=="03eb", ATTRS{idProduct}=="6124", MODE:="0666"
KERNEL=="ttyACM*", ATTRS{idVendor}=="03eb", ATTRS{idProduct}=="6124", MODE:="0666"

# Digistump boards
SUBSYSTEMS=="usb", ATTRS{idVendor}=="16d0", ATTRS{idProduct}=="0753", MODE:="0666"
KERNEL=="ttyACM*", ATTRS{idVendor}=="16d0", ATTRS{idProduct}=="0753", MODE:="0666", ENV{ID_MM_DEVICE_IGNORE}="1"

# STM32 discovery boards, with onboard st/linkv2
SUBSYSTEMS=="usb", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="374?", MODE:="0666"

# USBtiny
SUBSYSTEMS=="usb", ATTRS{idProduct}=="0c9f", ATTRS{idVendor}=="1781", MODE="0666"

# USBasp V2.0
SUBSYSTEMS=="usb", ATTRS{idVendor}=="16c0", ATTRS{idProduct}=="05dc", MODE:="0666"

# Teensy boards
ATTRS{idVendor}=="16c0", ATTRS{idProduct}=="04[789]?", ENV{ID_MM_DEVICE_IGNORE}="1"
ATTRS{idVendor}=="16c0", ATTRS{idProduct}=="04[789]?", ENV{MTP_NO_PROBE}="1"
SUBSYSTEMS=="usb", ATTRS{idVendor}=="16c0", ATTRS{idProduct}=="04[789]?", MODE:="0666"
KERNEL=="ttyACM*", ATTRS{idVendor}=="16c0", ATTRS{idProduct}=="04[789]?", MODE:="0666"

#TI Stellaris Launchpad
SUBSYSTEMS=="usb", ATTRS{idVendor}=="1cbe", ATTRS{idProduct}=="00fd", MODE="0666"

#TI MSP430 Launchpad
SUBSYSTEMS=="usb", ATTRS{idVendor}=="0451", ATTRS{idProduct}=="f432", MODE="0666"

# CMSIS-DAP compatible adapters
ATTRS{product}=="*CMSIS-DAP*", MODE="664", GROUP="plugdev"

# Black Magic Probe
SUBSYSTEM=="tty", ATTRS{interface}=="Black Magic GDB Server"
SUBSYSTEM=="tty", ATTRS{interface}=="Black Magic UART Port"
      '';
      destination = "/etc/udev/rules.d/98-platformio-udev.rules";
    };
  };
}
