{ config, lib, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    docker
    docker_compose
  ];

  # Docker
  virtualisation.docker.enable = true;

}
