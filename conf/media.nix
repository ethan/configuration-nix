{ config, lib, pkgs, ... }:

{
  hardware.pulseaudio.package = pkgs.pulseaudioFull.override { jackaudioSupport = true; };
  hardware.pulseaudio.enable = true;

#  services.mopidy = {
#    enable = true;
#    extensionPackages = [
#      pkgs.mopidy-local-sqlite
#      pkgs.mopidy-spotify
#      pkgs.mopidy-mopify
#      pkgs.mopidy-iris
#    ];
#    configuration = ''
#      [spotify]
#      enabled = true
#      debug = false
#      # username = eethann
#      # password = substanceD
#      # client_id = a069e2a8-b9e7-4367-bcaa-8f628ce980e9
#      # client_secret = qO16lESPXExRVt4VnnSVuoQeI2scJg6_b0uS-Mng9g4=
#      bitrate = 320
#
#      [mopify]
#      enabled = true
#      debug = false
#
#      [iris]
#      country=us
#      locale=en_US
#    '';
#    extraConfigFiles = ["/home/ethan/.config/mopidy/mopidy.conf"];
#  };
#  users.users.ethan.extraGroups = [ "mopidy" ];
  environment.systemPackages = with pkgs; [
    vlc
    mpd
    ffmpeg
    # sunvox
  ];
}
