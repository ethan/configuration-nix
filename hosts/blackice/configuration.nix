# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  nix.nixPath = [ "nixpkgs=/opt/nixpkgs" "nixos-config=/etc/nixos/configuration.nix" ];

  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ../../conf/tablet.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "nodev";
  boot.loader.grub.efiSupport = true;
  boot.loader.grub.gfxmodeEfi = "1024x768";

  boot.initrd.luks.devices = {
    "root" = {
      # device = "/dev/disk/by-uuid/ba9c1cc2-fa78-45b5-b8f5-7917012992d5";
      device = "/dev/nvme0n1p6";
      preLVM = true;
      # allowDiscards = true;
    };
  };

  boot.kernelModules = [ "kvm-intel" ];
  boot.kernelParams = [
    "pcie.aspm=force"
    "i915.enable_fbc=1"
    "i915.enable_rc6=7"
    "i915.lvds_downclock=1"
    "i915.enable_guc_loading=1"
    "i915.enable_guc_submission=1"
    "i915.enable_psr=0"
    "i915.enable_dpcd_backlight=1"
    "mem_sleep_default=s2idle"
  ];
# Recomendations from https://wiki.archlinux.org/index.php/Dell_XPS_13_2-in-1_(9365)#Suspend_issues
  boot.extraModprobeConfig = ''
    snd_hda_intel power_save=0
  '';

#  services.throttled.enable = true;

# Slightly more cautious than defaults
# See https://github.com/erpalma/throttled/blob/master/etc/lenovo_fix.conf
# TODO look into undervolting for the XPS 9365
#   services.throttled.extraConfig = ''
# [BATTERY]
# Trip_Temp_C: 70
# 
# [AC]
# Trip_Temp_C: 75
# '';

  fileSystems."/".options = [ "noatime" "nodiratime" "discard" ];
  
  services.fwupd.enable = true; 

  hardware.cpu.intel.updateMicrocode = true;

  powerManagement.enable = true;
  powerManagement.cpuFreqGovernor = null; # will be managed by tlphardware-configuration.nix

  networking.hostName = "geekchic"; # Define your hostname.
  networking.networkmanager = {
    enable = true;
    wifi.backend = "iwd";
  };
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  # networking.wireless.userControlled = true;
  networking.wireless.iwd.enable = true;
#  networking.networkmanager.extraConfig = ''
#  [device]
#  wifi.backend=iwd
#'';

  hardware.pulseaudio.extraConfig = ''
    load-module module-switch-on-connect
  '';
  hardware.bluetooth = {
    enable = true;
    powerOnBoot = true;
    # package = pkgs.bluezFull;
  };
  services.blueman.enable = true;


  services.logind.lidSwitch = "suspend";
  # environment.etc."systemd/sleep.conf".text = "HibernateDelaySec=8h";

  services.dbus.enable = true;
  services.upower.enable = true;
  services.tlp.enable = true;
  services.physlock = {
    allowAnyUser = true;
    enable = true;
  };
  services.acpid = {
    enable = true;
    powerEventCommands = "echo noop > /dev/null";
  };


  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  # i18n = {
  #   consoleFont = "Lat2-Terminus16";
  #   consoleKeyMap = "us";
  #   defaultLocale = "en_US.UTF-8";
  # };

  # Set your time zone.
  # time.timeZone = "Europe/Amsterdam";

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    wget vim git 
    acpilight
    # iw
    # wpa_gui
  ];

  hardware.acpilight.enable = true;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = { enable = true; enableSSHSupport = true; };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  # sound.enable = true;
  # hardware.pulseaudio.enable = true;

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";

  # Enable touchpad support.
  services.xserver.libinput.enable = true;
  fonts.fontconfig.dpi = 135;
  # i18n.consoleFont = "ter-i32b";
  i18n.consolePackages = with pkgs; [ terminus_font ];
  boot.earlyVconsoleSetup = true;
  boot.plymouth.enable = true;

  # Enable the KDE Desktop Environment.
  # services.xserver.displayManager.sddm.enable = true;
  # services.xserver.desktopManager.plasma5.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.ethan = {
    isNormalUser = true;
    extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  # system.stateVersion = "19.03"; # Did you read the comment?

}
